﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Track Package | tito Express Limited</title>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="description" content="Reliable, Safe and Fast Delivery" />
	<meta name="keywords" content="tito Express Limited" />
	<meta name="author" content="bondwebsolutions" />
	<link href="src/facebox.css" media="screen" rel="stylesheet" type="text/css" />
	<script src="src/facebox.js" type="text/javascript"></script>
	<link rel="stylesheet" type="text/css" href="pro_dropdown_2/pro_dropdown_2.css" />
	<script type="text/javascript" src="pro_dropdown_2/stuHover.js"></script>
	<script src="lib/jquery.js" type="text/javascript"></script>
	<link rel="stylesheet" type="text/css" href="andreas06.css" title="andreas06" media="screen,projection" />
    <style type="text/css">
        #Text1
        {
            text-align: left;
            width: 238px;
        }
    </style>
	<style type="text/css">
        .style1
        {
            color: #990000;
            font-weight: bold;
            font-size: medium;
        }
    </style>
        <script type="text/javascript">
    
    jQuery(document).ready(function($) {
      $('a[rel*=facebox]').facebox({
        loadingImage : 'src/loading.gif',
        closeImage   : 'src/closelabel.png'
      })
    })
  </script>
</head>
<body>
<div id="container">
<a id="top"></a><p class="hide">Skip to: <a href="#nav">site menu</a> | <a href="#leftside">section menu</a> | <a href="#content">main content</a></p>
<div id="sitename">
		<h1>tito Express Limited</h1>
		<span>Courier Service Company</span>
</div>
<div id="nav">
		<ul>
			<li><a href="index.htm">Home</a></li>
			<li><a href="#">About Us</a></li>
			<li><a href="#">Services</a></li>
			<li id="current"><a href="track.htm">Track</a></li>
			<li><a href="#">Support</a></li>
			<li><a href="#">Contact Us</a></li>
		</ul>
		<p class="hide"><a href="#top">Back to top</a></p>
	</div>
	<div id="wrap1">
	<div id="wrap2">
	<div id="topbox">
				<strong><span class="hide">Currently viewing: </span><a href="index.htm">tito Express Home</a> &raquo; <a href="index.htm">Front page</a></strong>
			</div>
			<div id="leftside">
			<p>
			    Laitan work</p>	

			</div>
			<div id="rightside">
				
			    Hello world<br />
                doo goo<br />
                Am going to
                <br />
                rttt<br />
                good day</div>
			<form action="tracklogin.php" method=POST name="mytrack" onsubmit="return validate()">
			<div id="content">
			<h2>Track Page</h2>
			<span class="style1">Wrong tracking number please try again!!!</span>
			<img src="img/gravatar-leaf.jpg" height="80" width="80" alt="Gravatar example" />
				<p class="intro">Our web-based track and trace system enables you to view the latest status on your shipment,  Check a shipment's status as it progresses through our hubs and scanning stations,  We're making technology work for you and your customers, anytime, anywhere
			</p>
				<p>To check on the delivery status of your package, enter your label or receipt number in the space provided. Please be sure to enter all letters  exactly as they appear.</p>
			    <p>
                    <b>- Track your Shipment -</b>
                </p>
                <p>
                    <input name ='trackmail' type="text" /><br /> 
                    <input name="submit" style="width: 80px" type="submit" value="Track" />
                    <input name="reset" style="width: 80px" type="reset" value="Clear" />
                        </p>

				<p class="hide"><a href="#top">Back to top</a></p>
			</div>
			</form>
	</div>
			<div id="footer">
			<p>&copy; 2012 tito Express S<a rel="facebox" href="login.php">o</a>lutions | design by <a href="http://bondwebsolutions.com/flash">bondwebsolutions</a><br />
			</p>
		</div>
	</div>
</div>
</body>
</html>
