��    <      �      �      �     �     �  $   �     #     3     ?  
   H     S     W     ^     f     w     �     �  
   �     �  	   �     �     �      �     �            S   .  *   �     �  	   �     �     �     �     �     �  	   �  
                1   (  	   Z     d     m     �  	   �  !   �     �     �  3   �          &  <   -     j     o  	   u  	        �  4   �     �     �  '   �       �       �	     �	  5   
     =
  
   L
     W
     g
     z
     �
     �
     �
     �
     �
     �
     �
  
   �
     
  
        %  &   6     ]     d  (   l  [   �  4   �     &     +     1  
   9     D     S     `  	   }  
   �     �     �  O   �                    1     P  &   ]     �      �  <   �     �  	             !  	   &  	   0     :     L  6   T     �     �  '   �     �   %s is invalid. %s is not a file. %s is oversized. Must be under %s MB %s is required. AOL Manager Ad Types Add New %s Ads All %s All Ads All Applications Application Detail Applications Apply Online Plugins Categories Category Create Ad Edit %s Edit Ad Fields with (*)  are compulsory. Filter Filter Dropdown%s - All Form ID is missing Form has been submitted successfully. If required, we will get back to you shortly! Invalid file %s. Allowed file types are %s Location Locations Name New %s New %s Name New Ad No applications found. Parent %s Parent %s: Pending Pending Applications Post ads and start receiving applications online. Read More Rejected Rejected Applications Salient Features Search %s Session Expired, please try again Shortlisted Shortlisted Applications Sorry, we could not find what you were looking for. Spider Teams Submit This is an automated response from Apply Online plugin on %s Type Types Undefined Update %s View %s We are no longer accepting applications for this ad. e.g. https://spiderteams.com https://www.linkedin.com/in/farhan-noor publicApply Online Project-Id-Version: Apply Online
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2019-04-27 23:01+0000
PO-Revision-Date: 2019-04-27 23:02+0000
Language-Team: French (France)
Plural-Forms: nplurals=2; plural=(n > 1);
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.2.2; wp-5.1.1
Last-Translator: Farhan Noor <farhan.noor@wpreloaded.com>
Language: fr_FR %s est invalide. %s n'est pas un fichier. %s est trop lourd. Il doit être en dessous de  %s MB %s est requis. Opérateur Types d'annonce Ajouter nouvel  %s Appels Tous %s tous les appels Toutes les applications Détail de l'application Candidatures Candidature en ligne plugin Catégories Catégorie Créer un Appel Éditer %s Éditer un Appel Les champs avec (*) sont obligatoires. Filtre %s Tous L'identifiant du formulaire est manquant Le formulaire a été soumis avec succès. Si nécessaire, nous vous contacterons sous peu! Fichier invalide %s. Les fichiers autorisés sont %s Lieu Lieux prénom Nouveau %s Nouveau %s Nom Nouvel Appel Aucune candidature trouvées Parent %s Parent %s: en attendant Demandes en attente Publiez des appels en ligne et commencez à recevoir des candidatures en ligne. En savoir plus Rejeté Candidatures rejetées Principales caractéristiques  Recherche %s Session expirée; veuillez recommencer Présélectionné Candidatures Présélectionnées Désolé, nous n'avons pas pu trouver ce que vous cherchiez. Spider Teams Soumettre réponse automatisée Type Les types Indéfini Mettre à jour %s Voir %s Nous n'acceptons plus les candidatures pour cet appel. par exemple https://spiderteams.com https://www.linkedin.com/in/farhan-noor Candidature en ligne 