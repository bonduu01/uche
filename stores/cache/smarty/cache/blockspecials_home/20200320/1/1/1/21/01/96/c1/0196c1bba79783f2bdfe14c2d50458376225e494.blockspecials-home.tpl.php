<?php /*%%SmartyHeaderCode:13459323405e74e3634a2353-18551611%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0196c1bba79783f2bdfe14c2d50458376225e494' => 
    array (
      0 => '/home/dsmgpvhc/public_html/stores/modules/blockspecials/views/templates/hook/blockspecials-home.tpl',
      1 => 1556676732,
      2 => 'file',
    ),
    'e2f940b214800eedd0ddbc18afa6239a53662d26' => 
    array (
      0 => '/home/dsmgpvhc/public_html/stores/themes/default-bootstrap/product-list.tpl',
      1 => 1556656932,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '13459323405e74e3634a2353-18551611',
  'variables' => 
  array (
    'specials' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5e74e3634b82e4_21032004',
  'cache_lifetime' => 31536000,
),true); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5e74e3634b82e4_21032004')) {function content_5e74e3634b82e4_21032004($_smarty_tpl) {?>		
									
		
	
	<!-- Products list -->
	<ul id="blockspecials" class="product_list grid row blockspecials tab-pane">
			
		
		
								<li class="ajax_block_product col-xs-12 col-sm-4 col-md-3 first-in-line last-line first-item-of-tablet-line first-item-of-mobile-line last-mobile-line">
			<div class="product-container" itemscope itemtype="https://schema.org/Product">
				<div class="left-block">
					<div class="product-image-container">
						<a class="product_img_link" href="https://dsmglobalservice.online/stores/index.php?id_product=5&amp;controller=product" title="Printed Summer Dress" itemprop="url">
							<img class="replace-2x img-responsive" src="https://dsmglobalservice.online/stores/img/p/1/2/12-home_default.jpg" alt="Printed Summer Dress" title="Printed Summer Dress"  width="250" height="250" itemprop="image" />
						</a>
													<div class="quick-view-wrapper-mobile">
							<a class="quick-view-mobile" href="https://dsmglobalservice.online/stores/index.php?id_product=5&amp;controller=product" rel="https://dsmglobalservice.online/stores/index.php?id_product=5&amp;controller=product">
								<i class="icon-eye-open"></i>
							</a>
						</div>
						<a class="quick-view" href="https://dsmglobalservice.online/stores/index.php?id_product=5&amp;controller=product" rel="https://dsmglobalservice.online/stores/index.php?id_product=5&amp;controller=product">
							<span>Quick view</span>
						</a>
																			<div class="content_price" itemprop="offers" itemscope itemtype="https://schema.org/Offer">
																	<span itemprop="price" class="price product-price">
										
										$28.98									</span>
									<meta itemprop="priceCurrency" content="USD" />
																			
										<span class="old-price product-price">
											$30.51
										</span>
																					<span class="price-percent-reduction">-5%</span>
																																						<span class="unvisible">
																								<link itemprop="availability" href="https://schema.org/InStock" />In stock																					</span>
																		
									
															</div>
																			<a class="new-box" href="https://dsmglobalservice.online/stores/index.php?id_product=5&amp;controller=product">
								<span class="new-label">New</span>
							</a>
																	</div>
										
				</div>
				<div class="right-block">
					<h5 itemprop="name">
												<a class="product-name" href="https://dsmglobalservice.online/stores/index.php?id_product=5&amp;controller=product" title="Printed Summer Dress" itemprop="url" >
							Printed Summer Dress
						</a>
					</h5>
															<p class="product-desc" itemprop="description">
						Long printed dress with thin adjustable straps. V-neckline and wiring under the bust with ruffles at the bottom of the dress.
					</p>
										<div class="content_price">
													
							<span class="price product-price">
								$28.98							</span>
															
								<span class="old-price product-price">
									$30.51
								</span>
								
																	<span class="price-percent-reduction">-5%</span>
																						
							
							
											</div>
										<div class="button-container">
																													<a class="button ajax_add_to_cart_button btn btn-default" href="https://dsmglobalservice.online/stores/index.php?controller=cart&amp;add=1&amp;id_product=5&amp;ipa=19&amp;token=9e9880cf7de87ae9f035c60ef5a7cc80" rel="nofollow" title="Add to cart" data-id-product-attribute="19" data-id-product="5" data-minimal_quantity="1">
									<span>Add to cart</span>
								</a>
																			<a class="button lnk_view btn btn-default" href="https://dsmglobalservice.online/stores/index.php?id_product=5&amp;controller=product" title="View">
							<span>More</span>
						</a>
					</div>
										<div class="product-flags">
																																	<span class="discount">Reduced price!</span>
												</div>
																		<span class="availability">
																	<span class=" label-success">
										In stock									</span>
															</span>
															</div>
							</div><!-- .product-container> -->
		</li>
			
		
		
								<li class="ajax_block_product col-xs-12 col-sm-4 col-md-3 last-line last-item-of-mobile-line last-mobile-line">
			<div class="product-container" itemscope itemtype="https://schema.org/Product">
				<div class="left-block">
					<div class="product-image-container">
						<a class="product_img_link" href="https://dsmglobalservice.online/stores/index.php?id_product=7&amp;controller=product" title="Printed Chiffon Dress" itemprop="url">
							<img class="replace-2x img-responsive" src="https://dsmglobalservice.online/stores/img/p/2/0/20-home_default.jpg" alt="Printed Chiffon Dress" title="Printed Chiffon Dress"  width="250" height="250" itemprop="image" />
						</a>
													<div class="quick-view-wrapper-mobile">
							<a class="quick-view-mobile" href="https://dsmglobalservice.online/stores/index.php?id_product=7&amp;controller=product" rel="https://dsmglobalservice.online/stores/index.php?id_product=7&amp;controller=product">
								<i class="icon-eye-open"></i>
							</a>
						</div>
						<a class="quick-view" href="https://dsmglobalservice.online/stores/index.php?id_product=7&amp;controller=product" rel="https://dsmglobalservice.online/stores/index.php?id_product=7&amp;controller=product">
							<span>Quick view</span>
						</a>
																			<div class="content_price" itemprop="offers" itemscope itemtype="https://schema.org/Offer">
																	<span itemprop="price" class="price product-price">
										
										$16.40									</span>
									<meta itemprop="priceCurrency" content="USD" />
																			
										<span class="old-price product-price">
											$20.50
										</span>
																					<span class="price-percent-reduction">-20%</span>
																																						<span class="unvisible">
																								<link itemprop="availability" href="https://schema.org/InStock" />In stock																					</span>
																		
									
															</div>
																			<a class="new-box" href="https://dsmglobalservice.online/stores/index.php?id_product=7&amp;controller=product">
								<span class="new-label">New</span>
							</a>
																	</div>
										
				</div>
				<div class="right-block">
					<h5 itemprop="name">
												<a class="product-name" href="https://dsmglobalservice.online/stores/index.php?id_product=7&amp;controller=product" title="Printed Chiffon Dress" itemprop="url" >
							Printed Chiffon Dress
						</a>
					</h5>
															<p class="product-desc" itemprop="description">
						Printed chiffon knee length dress with tank straps. Deep v-neckline.
					</p>
										<div class="content_price">
													
							<span class="price product-price">
								$16.40							</span>
															
								<span class="old-price product-price">
									$20.50
								</span>
								
																	<span class="price-percent-reduction">-20%</span>
																						
							
							
											</div>
										<div class="button-container">
																													<a class="button ajax_add_to_cart_button btn btn-default" href="https://dsmglobalservice.online/stores/index.php?controller=cart&amp;add=1&amp;id_product=7&amp;ipa=34&amp;token=9e9880cf7de87ae9f035c60ef5a7cc80" rel="nofollow" title="Add to cart" data-id-product-attribute="34" data-id-product="7" data-minimal_quantity="1">
									<span>Add to cart</span>
								</a>
																			<a class="button lnk_view btn btn-default" href="https://dsmglobalservice.online/stores/index.php?id_product=7&amp;controller=product" title="View">
							<span>More</span>
						</a>
					</div>
										<div class="product-flags">
																																	<span class="discount">Reduced price!</span>
												</div>
																		<span class="availability">
																	<span class=" label-success">
										In stock									</span>
															</span>
															</div>
							</div><!-- .product-container> -->
		</li>
		</ul>





<?php }} ?>
