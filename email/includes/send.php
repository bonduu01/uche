<?
	session_start();
	include_once('Swift/Swift.php');
	include_once('Swift/Swift/Connection/SMTP.php');
	require_once("../config.php");

	$fp = fopen("../$db_file", "r");
	$recipients = fread($fp, filesize("../$db_file"));
	$recipients = explode(",", $recipients);
	$mail_type 		= $_SESSION[type];
	$mail_subject	= $_SESSION[subject];
	$mail_from 		= "$admin_name <$admin_email>";
	$mail_body 		= stripslashes($_SESSION[message]);
	
	//print_r($recipients);
	//print $mail_from;
	//print $mail_subject;
	//print $mail_type;
	
	if($use_smtp == 'on')
	{
		$mailer = new Swift(new Swift_Connection_SMTP($smtp_host));
		
		if ($mailer->isConnected())
		{
			if ($mailer->authenticate($smtp_username, $smtp_password))
			{
				if($mail_type == 'text')
				{
					if($unsubscribe_link == 'on')
						$mail_body = "$mail_body\n--------------------------------------------\nclick on the following link to unsubscribe\n$site_url/$opennewsletter_dir/index.php?action=unsubscribe\n--------------------------------------------\n";
					
					$mailer->addPart($mail_body);
				}
				
				if($mail_type == 'html')
				{
					if($unsubscribe_link == 'on')
						$mail_body = "$mail_body<br><hr><a href=$site_url/$opennewsletter_dir/index.php?action=unsubscribe><font face='arial' size='2'>click here to unsubscribe</font></a><hr>";
					
					$mailer->addPart($mail_body, 'text/html');
				}
				
				if($mailer->send($recipients, $mail_from, $mail_subject))
					print "<font face='arial' size='2'>mail sent...</font>";
				else
					print "<font face='arial' size=2>mail could not be sent...</font>";
				$mailer->close();
			}
			else
			{
				print "<font face='arial' size=2>could not authenticate you the smtp server...</font>";
			}
		}
		else
		{
			print "<font face='arial' size=2>could not connect to the smtp server...</font>";
		}
	}
	else
	{
		foreach($recipients as $recipient)
		{
			if($mail_type == 'text')
			{
				if($unsubscribe_link == 'on')
					$body = "$mail_body\n--------------------------------------------\nclick on the following link to unsubscribe\n$site_url/$opennewsletter_dir/index.php?action=unsubscribe\n--------------------------------------------\n";
				
				$headers  = "MIME-Version: 1.0" . "\r\n";
				$headers .= "Content-type: text/plain; charset=\"$charset\"" . "\r\n";
				$headers .= "Return-Path: $mail_from" ." \r\n";
				$headers .= "Reply-To: $mail_from" . "\r\n";
				$headers .= "From: $mail_from" . "\r\n";
			}
			
			if($mail_type == 'html')
			{
				if($unsubscribe_link == 'on')
					$body = "$mail_body<br><hr><a href=$site_url/$opennewsletter_dir/index.php?action=unsubscribe><font face='arial' size='2'>click here to unsubscribe</font></a><hr>";

				$headers  = "MIME-Version: 1.0" . "\r\n";
				$headers .= "Content-type: text/html; charset=\"$charset\"" . "\r\n";
				$headers .= "Return-Path: $mail_from" ." \r\n";
				$headers .= "Reply-To: $mail_from" . "\r\n";
				$headers .= "From: $mail_from" . "\r\n";
			}
			
			if(mail($recipient, $mail_subject, $body, $headers))
				print "<font face='arial' size='2'>mail sent to $recipient...</font><br>";
			else
				print "<font face='arial' size='2'>mail could not be sent to $recipient...</font><br>";
		}
	}
?>