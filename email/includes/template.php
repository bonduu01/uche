<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>OpenNewsletter 2.5 : A Free &amp; Open-Source PHP Newsletter</title>
	<meta name="keywords" content="opennewsletter, OpenNewsletter, Open Newsletter, Open PHP Newsletter, open-source newsletter, free newsletter, free php newsletter, self exile, sohail abid, web helix, self.exile@gmail.com, creator of opennewsletter,">
	<meta name="description" content="OpenNewsletter is a free & open-source newsletter/mailing-list manager developed in php Self Exile. It does not require a database. You just need to put it in your website and its running!">
	<style>
	body, td, th { font-family: Arial; line-height:1.5; font-size:10pt; }
	a, a:visited { color:#002BB8; text-decoration:none; }
	a:hover		 { background-color:#DFE8FF; text-decoration:none; }

	.menu_header           { color: white; ont-weight: bold; text-decoration: none; text-align: center; vertical-align: middle }
	.menu_header a:link    { color: white; background-color:#4466BB; display: block; padding: 1px; border:1px solid #5f81d7; }
	.menu_header a:visited { color: white; background-color:#4466BB; display: block; padding: 1px; border:1px solid #5f81d7; }
	.menu_header a:hover   { color: white; background-color:#5f81d7; display: block; padding: 1px; border:1px solid #5f81d7; }
	.menu_header a:active  { color: white; background-color:#4466BB; display: block; padding: 1px; border:1px solid #5f81d7; }
	a.menu2, a.menu2:visited{ color:#FFFFFF; background-color:#5f81d7; }
	a.menu2:hover			{ color:#FFFFFF; background-color:#5f81d7; }
	
	h1, h2, h3, h4 { font-weight:normal; }
	hr 			{ color:#DFE8FF; height:1px; }
	input, textarea, select { font-family:Arial; font-size:12px; }
	
	.textField 	{ width:200px; border:1px solid #CCCCCC; }
	.textField2 { width:300px; border:1px solid #CCCCCC; }
	.textField3 { width:100px; border:1px solid #CCCCCC; }
	.button 	{ background-image:url(images/bg_button.gif); width:90px; height:21px; color:#4466BB; border:0; font-weight:bold; }
	.copyright 	{ font-size:9pt; }
	.heading2 	{ font-size:16px; font-weight:bold; }
	.heading	{ font-size:18px; width:100%; border-bottom:1px solid #CCCCCC; text-align:left; margin-bottom:10px; }
	.message	{ background-color:#efefef; padding:4px; border:1px solid #CCCCCC; text-align:center; }
	.error		{ background-color:#efefef; padding:4px; border:1px solid #CCCCCC; text-align:center; }
	.error_text	{ color:#FF0000; }
</style>
<script language="JavaScript" type="text/javascript" src="includes/gen_validatorv2.js"></script>
<?php
if($page == "compose" && $_GET["type"]=="html")
{
?>
<!-- tinyMCE -->
<script language="javascript" type="text/javascript" src="includes/tiny_mce/tiny_mce.js"></script>
<script language="javascript" type="text/javascript">

	tinyMCE.init({
		mode : "textareas",
		theme : "advanced",
		convert_urls : false,
		theme_advanced_buttons1 : "fontselect,fontsizeselect,formatselect,separator,bold,italic,underline,strikethrough,separator,sup,sub,separator,cut,copy,paste,separator,undo,redo",
		theme_advanced_buttons2 : "justifyleft,justifycenter,justifyright,justifyfull,separator,bullist,numlist,separator,outdent,indent,separator,forecolor,backcolor,separator,hr,link,unlink,anchor,image,table,separator,charmap,code",
		theme_advanced_buttons3 : "",
 		theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        plugins : "advimage,table,",
		debug : false
	});
</script>
<!-- /tinyMCE -->
<?php
}
?>
</head>

<body bgcolor='#f3f3f3'>
<table align=center width='750' border='0' cellpadding='0' cellspacing='0' background='images/content_bkg.gif'>
<tr><td><img src='images/content_cap_top.gif' width='750' height='8'></td></tr>
<tr>
	<td style='padding-top:5px;padding-left:20px;padding-bottom:10px;padding-right:20px;'>
	
	<table width="100%" cellpadding="0" cellspacing="0" cellpadding="0" style="border-bottom:1px solid #CCCCCC; margin-bottom:5px; ">
	<tr>
	<td width="270" valign="bottom">
	<img src="images/logo.gif" alt="OpenNewsletter">
	</td>
	<td align="left" valign="bottom" style="padding-bottom:2px; font-size:11px;">
	<a href='http://selfexile.com/'>Project Home Page</a> | 
	<a href='http://selfexile.com/projects/opennewsletter/'>Support Forum</a>
	<?
	if($_SESSION["valid"] == true)
	{
	echo " | <a href='index.php?action=logout'>Logout</a>";
	}
	?>
	</td>
	</tr>
	</table>

	<table class="menu_header" cellpadding="0" width="100%" cellspacing="0" style='text-align:center; table-layout:fixed'>
	<tr>
	<?
		if($page == "home")
			echo "<td><a class=menu2 href='index.php'>Home</a></td>";
		else
			echo "<td><a href='index.php'>Home</a></td>";
		
		if($page == "subscribers")
			echo "<td><a class=menu2 href='subscribers.php'>Subscribers</a></td>";
		else
			echo "<td><a href='subscribers.php'>Subscribers</a></td>";
		
		if($page == "compose")
			echo "<td><a class=menu2 href='compose.php?type=text'>Compose</a></td>";
		else
			echo "<td><a href='compose.php?type=text'>Compose</a></td>";
		
		if($page == "settings")
			echo "<td><a class=menu2 href='settings.php'>Settings</a></td>";
		else
			echo "<td><a href='settings.php'>Settings</a></td>";
			
		if($page == "integration")
			echo "<td><a class=menu2 href='index.php?action=help'>Help Guide</a></td>";
		else
			echo "<td><a href='index.php?action=help'>Help Guide</a></td>";
	?>
		
	</tr>
	</table>		
	
	<table width="100%" cellpadding="0" cellspacing="0" style="margin-top:5px;margin-bottom:5px;">
	<tr>
	<td>
		<div style="border:1px solid #CCCCCC; padding:10px; padding-top:5px; text-align:center">
			<? echo $main; ?>
		</div>
	</td>
	</tr>
	</table>
	
	<div style="border:1px solid #CCCCCC; padding:3px;" align="center" class="copyright">
	This is a <a href="index.php?action=fsd">Free</a> & <a href="index.php?action=osd">Open Source</a> php newsletter developed by <a target="_blank" href="http://selfexile.com/">Sohail Abid [self exile]</a> and distributed under the <a href="index.php?action=gpl">GPL</a>.
	</div>
	
	
	</td>
</tr>
<tr><td><img src='images/content_cap_bottom.gif' width='750' height='12'></td></tr>
</table>
<div style="display:none">
OpenNewsletter is a free & open-source newsletter/mailing-list manager written in php developed by Self Exile. It does not require a database. You just need to put it in your website and its running!
I am Sohail Abid code name Self Exile, the developer of OpenNewletter. I created it because I like beautiful but simple things. You can visit my home page at www.selfexile.com. And the home page fo this software is now: www.selfexile.com/projects/opennewsletter. My email address is self.exile at gmail.com and my sf project page is sourceforge.net/projects/opennewsletter
</div>
</body>
</html>