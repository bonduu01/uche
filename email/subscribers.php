<?
	session_start();
	require_once("config.php");
	
	if($_SESSION["valid"] != true)
	{
	    header("Location: index.php");
	    exit(0);
	}
	
	if($_GET["action"] == "add")
	{
		$fp = fopen("$db_file", "r");
		$file_text = fread($fp, 999999);
		fclose($fp);
		
		$subscribers = explode(",",$file_text);
		foreach($subscribers as $subscriber)
		{
			if($subscriber == $_POST["email"])
			{
				$result = 1;
				break;
			}
			else
			{
				$result = 0;
			}
		}
		
		if($result == 1)
		{
			$msg = "<blockquote><div class=error>Cannot add subscriber, subscriber already exists...</div></blockquote>";
		}
		else
		{
			$fp = fopen("$db_file", "a+");
			fwrite($fp, $_POST["email"] . ",");
			fclose($fp);
			$msg = "<blockquote><div class=message>Subscriber added successfully...</div></blockquote>";
		}
	}
	
	if($_GET["action"] == "delete")
	{	
		$fp = fopen("$db_file", "r");
		$file_text = fread($fp, 999999);
		fclose($fp);
		
		$fp = fopen("$db_file", "w");
		$file_text_new = str_replace("$_POST[email],", "", $file_text);
		fwrite($fp, $file_text_new);
		fclose($fp);
		$msg = "<blockquote><div class=message>Subscriber deleted successfully...</div></blockquote>";
	}
	
	$main .= "
			<div class=heading>Manage Subscribers</div>
			$msg
			<table width=100%>
			
			<tr>
			<td align=center>
			<div class=heading2>add a subscriber</div><br>
			<form name=add action='subscribers.php?action=add' method='post'>
			Email<br>
			<input class=textField type=text name=email title='Please enter a valid email address...'>
			<br>
			<span class=error_text id=label_email></span>
			<br>
			<input class=button type=submit value=Add>
			</form>
			<script language=javascript>
				var validator  = new Validator('add');
				validator.addValidation('email','req','Please enter a valid email');
				validator.addValidation('email','email','Please enter a valid email');
			</script>
			</td>
			
			
			<td align=center>
				<div class=heading2>delete a subscriber</div><br>
				<form action='subscribers.php?action=delete' method='post'>
				Email<br>
				<select class=textField name=email>";
				$fp = fopen("$db_file", "r");
				while (!feof($fp))
				{
					$char = fread($fp, 1);
					if($char == ",")
					{
						$main .= "<option>$buffer</option>";
						$buffer = "";
					}
					else
					{
						$buffer .= "$char";
					}
				}
				fclose($fp);
				
			 $main .= "</select><br><br>
			 <input class=button type=submit value=Delete>
			 </form>
			 </td>
			 </tr>
			 </table>
			 
			 
			 ";
	
	$page = "subscribers";
	require_once("includes/template.php");
?>