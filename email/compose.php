<?
	session_start();
	require_once("config.php");
	
	if($_SESSION["valid"] != true)
	{
	    header("Location: index.php");
	    exit(0);
	}
	
	if($_POST["submit"] == "Preview")
	{
		$message = stripslashes($_POST[message]);
		$main .= "
			<div class=heading>Compose Newsletter</div>
			$msg
			<form name=form action='compose.php?type=$_GET[type]' method='post'>
			<table width=100% cellpadding=5px >
			<tr bgcolor='#f9f9f9'>
				<td align=left><b>Type</b>: <div style='background-color:white;'>$_GET[type]</div></td>
			</tr>
			<tr bgcolor='#f4f4f4'>
				<td align=left><b>From</b>: <div style='background-color:white;'>$admin_name &lt;$admin_email&gt;</div></td>
			</tr>
			<tr bgcolor='#f9f9f9'>
				<td align=left><b>Subject</b>: <div style='background-color:white;'>$_POST[subject]</div></td>
			</tr>
			<tr bgcolor='#f4f4f4'>
				<td align=left><b>Message</b>:<br><div style='background-color:white;'>$message</div></td>
			</tr>
			<tr bgcolor='#f9f9f9'>
				<td>
				<input name=submit class=button type=submit value=Modify>
				<input name=submit class=button type=submit value=Send>
				</td>
			</tr>
			</table>
			
			<input type='hidden' name='subject' value='$_POST[subject]'>
			<input type='hidden' name='type' value='$_POST[type]'>
			<input type='hidden' name='message' value='$_POST[message]'>
			</form>
			<script language=javascript>
			var validator  = new Validator('form');
			validator.addValidation('subject','req','');
			validator.addValidation('message','req','');
			</script>
			";

	}
	elseif($_POST["submit"] == "Send")
	{
		$_SESSION[subject] 	= $_POST[subject];
		$_SESSION[type] 	= $_GET[type];
		$_SESSION[message] 	= $_POST[message];
		
		$main .= "
			<div class=heading>Sending Newsletter</div>
			<br>
			<iframe name='sendframe' src='includes/send.php' frameborder='1' width=600 height=200></iframe>
			";
	}
	else
	{
		$main .= "
			<div class=heading>Compose Newsletter</div>
			$msg
			<form name=form action='compose.php?type=$_GET[type]' method='post'>
			<table width=100% cellpadding=5px>
			<tr bgcolor='#f9f9f9'>
				<td><b>Subject</b>:<br>
				<input class=textField2 type='text' value='$_POST[subject]' name='subject' title='Please enter a subject for the newsletter.'><br><span class=error_text id=label_subject></span></td>
			</tr>
			<tr bgcolor='#f3f3f3'>
				<td><b>From</b>:<br>
				<input class=textField2 disabled type='text' name='email_from' value='$admin_name <$admin_email>'></td>
			</tr>
			<tr bgcolor='#f9f9f9'>
				<td><b>Type</b>:<br>
				<a href='compose.php?type=text'>text</a>&nbsp;&nbsp;<a href='compose.php?type=html'>html</a></td>
			</tr>
			<tr bgcolor='#f3f3f3'>
				<td><b>Message</b>:<br>
				<textarea rows=25 cols='125' name='message' title='Please write something in the message area.'>$_POST[message]</textarea><span class=error_text id=label_message></span></td>
			</tr>
			<tr bgcolor='#f9f9f9'>
				<td>
				<input name=submit class=button type=submit value=Preview>
				<input name=submit class=button type=submit value=Send>
				</td>
			</tr>
			</table>
			</form>
			<script language=javascript>
			var validator  = new Validator('form');
			validator.addValidation('subject','req','');
			</script>
			";
	}
	
	$page = "compose";
	require_once("includes/template.php");
?>