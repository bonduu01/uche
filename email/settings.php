<?
	session_start();
	
	if($_SESSION["valid"] != true)
	{
	    header("Location: index.php");
	    exit(0);
	}
	
	if($_POST["action"] == "update")
	{	
		$fp = fopen("config.php", "w");
		$file_text = "<?
	\$admin_username = '$_POST[admin_username]';
	\$admin_password = '$_POST[admin_password]';
	\$admin_name = '$_POST[admin_name]';
	\$admin_email = '$_POST[admin_email]';

	\$use_smtp = '$_POST[use_smtp]';
	\$smtp_host = '$_POST[smtp_host]';
	\$smtp_username = '$_POST[smtp_username]';
	\$smtp_password = '$_POST[smtp_password]';
	
	\$charset = '$_POST[charset]';
	\$unsubscribe_link = '$_POST[unsubscribe_link]';
	\$site_url = '$_POST[site_url]';
	\$opennewsletter_dir = '$_POST[opennewsletter_dir]';
	\$db_file = '$_POST[db_file]';
?>";
		fwrite($fp, $file_text);
		fclose($fp);

		$fp = fopen($_POST[old_db_file], "r");
		$file_text = fread($fp, 999999);
		fclose($fp);
		
		rename($_POST[old_db_file],$_POST[db_file]);
		
		$msg = "<blockquote><div class=message>Settings updated successfully...</div></blockquote>";
	}
	
	require_once("config.php");
	
	if($use_smtp == 'on') $un1 = 'checked';
	if($unsubscribe_link == 'on') $un2 = 'checked';
	
	$main .= "
<div class=heading>Settings</div>
$msg
<table width='100%' cellpadding=2 cellspacing=2>
<form name=form action='settings.php' method='post'>
<input type=hidden name=action value=update>
<tr bgcolor='#f9f9f9'>
<td align=right width=150>Admin Username</td>
<td align=left><input class=textField type=text name='admin_username' value='$admin_username' title='Please enter username.'><span class=error_text id=label_admin_username></span></td>
<tr bgcolor='#f4f4f4'>
<td align=right>Admin Password</td>
<td align=left><input class=textField type=text name='admin_password' value='$admin_password' title='Please enter password.'><span class=error_text id=label_admin_password></span></td>
</tr>
<tr bgcolor='#f9f9f9'>
<td align=right>Admin Name</td>
<td align=left><input class=textField type=text name='admin_name' value='$admin_name' title='Please enter name.'><span class=error_text id=label_admin_name></span></td>
</tr>
<tr bgcolor='#f4f4f4'>
<td align=right>Admin Email</td>
<td align=left><input class=textField type=text name='admin_email' value='$admin_email' title='Please enter a valid email address.'><span class=error_text id=label_admin_email></span></td>
</tr>
<tr><td></td></tr>
<tr bgcolor='#f9f9f9'>
<td align=right>Use SMTP Server</td>
<td align=left><input type=checkbox $un1 name='use_smtp'> [if not checked, php's mail function will be used]</td>
</tr>
<tr bgcolor='#f4f4f4'>
<td align=right>SMTP Host</td>
<td align=left><input class=textField type=text name='smtp_host' value='$smtp_host'> (usually localhost)</td>
</tr>
<tr bgcolor='#f9f9f9'>
<td align=right>SMTP Username</td>
<td align=left><input class=textField type=text name='smtp_username' value='$smtp_username'> (usually your email address)</td>
</tr>
<tr bgcolor='#f4f4f4'>
<td align=right>SMTP Password</td>
<td align=left><input class=textField type=text name='smtp_password' value='$smtp_password'> (your email password)</td>
</tr>
<tr><td></td></tr>
<tr bgcolor='#f9f9f9'>
<td align=right>Newsletter Charset</td>
<td align=left><input class=textField type=text name='charset' value='$charset' title='Please enter a valid Newsletter Charset.'><span class=error_text id=label_charset></span></td>
</tr>
<tr bgcolor='#f4f4f4'>
<td align=right>Site URL</td>
<td align=left><input class=textField type=text name='site_url' value='$site_url' title='Please enter a valid URL for site.'><span class=error_text id=label_site_url></span></td>
</tr>
<tr bgcolor='#f9f9f9'>
<td align=right>OpenNewsletter Directory</td>
<td align=left><input class=textField type=text name='opennewsletter_dir' value='$opennewsletter_dir' title='Please enter a valid directory name for OpenNewsletter.'><span class=error_text id=label_opennewsletter_dir></span></td>
</tr>
<tr bgcolor='#f4f4f4'>
<td align=right>Database File</td>
<td align=left><input type=hidden name=old_db_file value=$db_file><input class=textField type=text name='db_file' value='$db_file' title='Please enter a valid db file name.'><span class=error_text id=label_db_file></span></td>
</tr>
<tr bgcolor='#f9f9f9'>
<td align=right>Send Unsubscribe Link</td>
<td align=left><input type=checkbox $un2 name='unsubscribe_link' title='Please enter a valid Newsletter Charset.'></td>
</tr>
<tr bgcolor='#f4f4f4'>
<td>&nbsp;</td>
<td align=left><input class=button type=submit value=Update></td>
</tr>
</form>
</table>
<script language=javascript>
var validator  = new Validator('form');
validator.addValidation('admin_username','req','');
validator.addValidation('admin_password','req','');
validator.addValidation('admin_name','req','');
validator.addValidation('admin_email','req','');
validator.addValidation('admin_email','email','');
validator.addValidation('charset','req','');
validator.addValidation('site_url','req','');
validator.addValidation('opennewsletter_dir','req','');
validator.addValidation('db_file','req','');
</script>
";
	
$page = "settings";
require_once("includes/template.php");
?>