<?
	session_start();
	require_once("config.php");
	
	if($_GET[action] == 'login')
	{
		if(isset($_POST["username"])) $username = $_POST["username"]; else $username = "";
		if(isset($_POST["password"])) $password = $_POST["password"]; else $password = "";
		
		if($admin_username == $username && $admin_password == $password)
		{
			$_SESSION['valid'] = true;
			header("Location: index.php");
		}
		else
		{
			$_SESSION['valid'] = false;
			header("Location: index.php?msg=error");
		}	
	}
	elseif($_GET[action] == 'logout')
	{
		$_SESSION['valid'] = false;
		header("Location: index.php");
	}
	elseif($_GET[action] == 'unsubscribe')
	{
		$main = "<div class=heading>Unsubscribe</div>
		<div align=justify>
		We are sorry to see you go. But you have full freedom to do so. We respect your wishes.<br><br>
		<form name=form method=post action='index.php?action=dounsubscribe'>
		Please enter your email address below and press \"Unsubscribe\"
		<br><br>
		<input class=textField type=text name=email title='Please enter a valid email address...'>&nbsp;&nbsp;<input type=submit name=submit class=button value='Unsubscribe'>
		<br>
		<span class=error_text id=label_email></span>
		</form>
		<script language=javascript>
			var validator  = new Validator('form');
			validator.addValidation('email','req','Please enter a valid email');
			validator.addValidation('email','email','Please enter a valid email');
		</script>	
		</div>
		<br><br>";
	}
	elseif($_GET[action] == 'dounsubscribe')
	{
		$fp = fopen("$db_file", "r");
		$file_text = fread($fp, 999999);
		fclose($fp);
		
		$fp = fopen("$db_file", "w");
		$file_text_new = str_replace("$_POST[email],", "", $file_text);
		fwrite($fp, $file_text_new);
		fclose($fp);
		
		$main = "<div class=heading>Unsubscribe</div>
		<div align=justify>
		You have been unsubscribed successfully...
		</div>";
	}
	elseif($_GET[action] == 'fsd')
	{
			$main = "<div class=heading>The Free Software Definition</div>
			<div align=justify>
			<p>We maintain this free software definition to show clearly what must be true about a particular software program for it to be considered free software. </p>
			<p>\"Free software\" is a matter of liberty, not price. To understand the concept, you should think of \"free\" as in \"free speech,\" not as in \"free beer.\" </p>
			<p>Free software is a matter of the users' freedom to run, copy, distribute, study, change and improve the software. More precisely, it refers to four kinds of freedom, for the users of the software: </p>
			<ul>
			  <li>The freedom to run the program, for any purpose (freedom 0).
			  <li>The freedom to study how the program works, and adapt it to your needs (freedom 1). Access to the source code is a precondition for this.
			  <li>The freedom to redistribute copies so you can help your neighbor (freedom 2).
			  <li>The freedom to improve the program, and release your improvements to the public, so that the whole community benefits (freedom 3). Access to the source code is a precondition for this. </li>
			</ul>
			<p>A program is free software if users have all of these freedoms. Thus, you should be free to redistribute copies, either with or without modifications, either gratis or charging a fee for distribution, to anyone anywhere. Being free to do these things means (among other things) that you do not have to ask or pay for permission. </p>
			<p>You should also have the freedom to make modifications and use them privately in your own work or play, without even mentioning that they exist. If you do publish your changes, you should not be required to notify anyone in particular, or in any particular way. </p>
			<p>The freedom to use a program means the freedom for any kind of person or organization to use it on any kind of computer system, for any kind of overall job, and without being required to communicate subsequently with the developer or any other specific entity. </p>
			<p>The freedom to redistribute copies must include binary or executable forms of the program, as well as source code, for both modified and unmodified versions. (Distributing programs in runnable form is necessary for conveniently installable free operating systems.) It is ok if there is no way to produce a binary or executable form for a certain program (since some languages don't support that feature), but you must have the freedom to redistribute such forms should you find or develop a way to make them. </p>
			<p>In order for the freedoms to make changes, and to publish improved versions, to be meaningful, you must have access to the source code of the program. Therefore, accessibility of source code is a necessary condition for free software. </p>
			<p>One important way to modify a program is by merging in available free subroutines and modules. If the program's license says that you cannot merge in an existing module, such as if it requires you to be the copyright holder of any code you add, then the license is too restrictive to qualify as free. </p>
			<p>In order for these freedoms to be real, they must be irrevocable as long as you do nothing wrong; if the developer of the software has the power to revoke the license, without your doing anything to give cause, the software is not free. </p>
			<p>However, certain kinds of rules about the manner of distributing free software are acceptable, when they don't conflict with the central freedoms. For example, copyleft (very simply stated) is the rule that when redistributing the program, you cannot add restrictions to deny other people the central freedoms. This rule does not conflict with the central freedoms; rather it protects them. </p>
			<p>You may have paid money to get copies of free software, or you may have obtained copies at no charge. But regardless of how you got your copies, you always have the freedom to copy and change the software, even to <a href='http://www.gnu.org/philosophy/selling.html'>sell copies </a>. </p>
			<p>\"Free software\" does not mean \"non-commercial\". A free program must be available for commercial use, commercial development, and commercial distribution. Commercial development of free software is no longer unusual; such free commercial software is very important. </p>
			<p>Rules about how to package a modified version are acceptable, if they don't substantively block your freedom to release modified versions, or your freedom to make and use modified versions privately. Rules that \"if you make your version available in this way, you must make it available in that way also\" can be acceptable too, on the same condition. (Note that such a rule still leaves you the choice of whether to publish your version at all.) Rules that require release of source code to the users for versions that you put into public use are also acceptable. It is also acceptable for the license to require that, if you have distributed a modified version and a previous developer asks for a copy of it, you must send one, or that you identify yourself on your modifications. </p>
			<p>In the GNU project, we use <a href='http://www.gnu.org/copyleft/copyleft.html'>\"copyleft\" </a> to protect these freedoms legally for everyone. But <a href='http://www.gnu.org/philosophy/categories.html#Non-CopyleftedFreeSoftware'>non-copylefted free software </a> also exists. We believe there are important reasons why <a href='http://www.gnu.org/philosophy/pragmatic.html'>it is better to use copyleft </a>, but if your program is non-copylefted free software, we can still use it. </p>
			<p>See <a href='http://www.gnu.org/philosophy/categories.html'>Categories of Free Software </a> for a description of how \"free software,\" \"copylefted software\" and other categories of software relate to each other. </p>
			<p>Sometimes government export control regulations and trade sanctions can constrain your freedom to distribute copies of programs internationally. Software developers do not have the power to eliminate or override these restrictions, but what they can and must do is refuse to impose them as conditions of use of the program. In this way, the restrictions will not affect activities and people outside the jurisdictions of these governments. </p>
			<p>Most free software licenses are based on copyright, and there are limits on what kinds of requirements can be imposed through copyright. If a copyright-based license respects freedom in the ways described above, it is unlikely to have some other sort of problem that we never anticipated (though this does happen occasionally). However, some free software licenses are based on contracts, and contracts can impose a much larger range of possible restrictions. That means there are many possible ways such a license could be unacceptably restrictive and non-free. </p>
			<p>We can't possibly list all the possible contract restrictions that would be unacceptable. If a contract-based license restricts the user in an unusual way that copyright-based licenses cannot, and which isn't mentioned here as legitimate, we will have to think about it, and we will probably decide it is non-free. </p>
			<p>When talking about free software, it is best to avoid using terms like \"give away\" or \"for free\", because those terms imply that the issue is about price, not freedom. Some common terms such as \"piracy\" embody opinions we hope you won't endorse. See <a href='http://www.gnu.org/philosophy/words-to-avoid.html'>Confusing Words and Phrases that are Worth Avoiding </a> for a discussion of these terms. We also have a list of <a href='http://www.gnu.org/philosophy/fs-translations.html'>translations of 'free software' </a> into various languages. </p>
			<p>Finally, note that criteria such as those stated in this free software definition require careful thought for their interpretation. To decide whether a specific software license qualifies as a free software license, we judge it based on these criteria to determine whether it fits their spirit as well as the precise words. If a license includes unconscionable restrictions, we reject it, even if we did not anticipate the issue in these criteria. Sometimes a license requirement raises an issue that calls for extensive thought, including discussions with a lawyer, before we can decide if the requirement is acceptable. When we reach a conclusion about a new issue, we often update these criteria to make it easier to see why certain licenses do or don't qualify. </p>
			<p>If you are interested in whether a specific license qualifies as a free software license, see our <a href='http://www.gnu.org/licenses/license-list.html'>list of licenses </a>. If the license you are concerned with is not listed there, you can ask us about it by sending us email at <a href='mailto:licensing@gnu.org'>&lt;licensing@gnu.org&gt; </a>. </p>
			<p>If you are contemplating writing a new license, please contact the FSF by writing to that address. The proliferation of different free software licenses means increased work for users in understanding the licenses; we may be able to help you find an existing Free Software license that meets your needs. </p>
			<p>If that isn't possible, if you really need a new license, with our help you can ensure that the license really is a Free Software license and avoid various practical problems. </p>
			</div>";
	}	
	elseif($_GET[action] == 'osd')
	{
		$main = "<div class=heading>The Open Source Definition</div>
		<div align=justify>
		<p align=center>Version 1.9 </p>
		<p><i>The indented, italicized sections below appear as annotations to the Open Source Definition (OSD) and are <b>not </b> a part of the OSD. </i></p>
		<h2>Introduction </h2>
		<p>Open source doesn't just mean access to the source code. The distribution terms of open-source software must comply with the following criteria:  
		<a name='1'>
		<h3>1. Free Redistribution </h3>
		</a>
		<p>The license shall not restrict any party from selling or giving away the software as a component of an aggregate software distribution containing programs from several different sources. The license shall not require a royalty or other fee for such sale. </p>
		<p><i><b>Rationale: </b> By constraining the license to require free redistribution, we eliminate the temptation to throw away many long-term gains in order to make a few short-term sales dollars. If we didn't do this, there would be lots of pressure for cooperators to defect. </i></p>
		<a name='2'>
		<h3>2. Source Code </h3>
		</a>
		<p>The program must include source code, and must allow distribution in source code as well as compiled form. Where some form of a product is not distributed with source code, there must be a well-publicized means of obtaining the source code for no more than a reasonable reproduction cost�preferably, downloading via the Internet without charge. The source code must be the preferred form in which a programmer would modify the program. Deliberately obfuscated source code is not allowed. Intermediate forms such as the output of a preprocessor or translator are not allowed. </p>
		<p><i><b>Rationale: </b> We require access to un-obfuscated source code because you can't evolve programs without modifying them. Since our purpose is to make evolution easy, we require that modification be made easy. </i></p>
		<a name='3'>
		<h3>3. Derived Works </h3>
		</a>
		<p>The license must allow modifications and derived works, and must allow them to be distributed under the same terms as the license of the original software. </p>
		<p><i><b>Rationale: </b> The mere ability to read source isn't enough to support independent peer review and rapid evolutionary selection. For rapid evolution to happen, people need to be able to experiment with and redistribute modifications. </i></p>
		<a name='4'>
		<h3>4. Integrity of The Author's Source Code </h3>
		</a>
		<p>The license may restrict source-code from being distributed in modified form only if the license allows the distribution of 'patch files' with the source code for the purpose of modifying the program at build time. The license must explicitly permit distribution of software built from modified source code. The license may require derived works to carry a different name or version number from the original software. </p>
		<p><i><b>Rationale: </b> Encouraging lots of improvement is a good thing, but users have a right to know who is responsible for the software they are using. Authors and maintainers have reciprocal right to know what they're being asked to support and protect their reputations. </i></p>
		<p><i>Accordingly, an open-source license <b>must </b> guarantee that source be readily available, but <b>may </b> require that it be distributed as pristine base sources plus patches. In this way, 'unofficial' changes can be made available but readily distinguished from the base source. </i></p>
		<a name='5'>
		<h3>5. No Discrimination Against Persons or Groups </h3>
		</a>
		<p>The license must not discriminate against any person or group of persons. </p>
		<p><i><b>Rationale: </b> In order to get the maximum benefit from the process, the maximum diversity of persons and groups should be equally eligible to contribute to open sources. Therefore we forbid any open-source license from locking anybody out of the process. </i></p>
		<p><i>Some countries, including the United States, have export restrictions for certain types of software. An OSD-conformant license may warn licensees of applicable restrictions and remind them that they are obliged to obey the law; however, it may not incorporate such restrictions itself. </i></p>
		<a name='6'>
		<h3>6. No Discrimination Against Fields of Endeavor </h3>
		</a>
		<p>The license must not restrict anyone from making use of the program in a specific field of endeavor. For example, it may not restrict the program from being used in a business, or from being used for genetic research. </p>
		<p><i><b>Rationale: </b> The major intention of this clause is to prohibit license traps that prevent open source from being used commercially. We want commercial users to join our community, not feel excluded from it. </i></p>
		<a name='7'>
		<h3>7. Distribution of License </h3>
		</a>
		<p>The rights attached to the program must apply to all to whom the program is redistributed without the need for execution of an additional license by those parties. </p>
		<p><i><b>Rationale: </b> This clause is intended to forbid closing up software by indirect means such as requiring a non-disclosure agreement. </i></p>
		<a name='8'>
		<h3>8. License Must Not Be Specific to a Product </h3>
		</a>
		<p>The rights attached to the program must not depend on the program's being part of a particular software distribution. If the program is extracted from that distribution and used or distributed within the terms of the program's license, all parties to whom the program is redistributed should have the same rights as those that are granted in conjunction with the original software distribution. </p>
		<p><i><b>Rationale: </b> This clause forecloses yet another class of license traps. </i></p>
		<a name='9'>
		<h3>9. License Must Not Restrict Other Software </h3>
		</a>
		<p>The license must not place restrictions on other software that is distributed along with the licensed software. For example, the license must not insist that all other programs distributed on the same medium must be open-source software. </p>
		<p><i><b>Rationale: </b> Distributors of open-source software have the right to make their own choices about their own software. </i></p>
		<p><i>Yes, the GPL is conformant with this requirement. Software linked with GPLed libraries only inherits the GPL if it forms a single work, not any software with which they are merely distributed. </i></p>
		<a name='10'>
		<h3>10. License Must Be Technology-Neutral </h3>
		</a>
		<p>No provision of the license may be predicated on any individual technology or style of interface. </p>
		<p><i><b>Rationale: </b> This provision is aimed specifically at licenses which require an explicit gesture of assent in order to establish a contract between licensor and licensee. Provisions mandating so-called 'click-wrap' may conflict with important methods of software distribution such as FTP download, CD-ROM anthologies, and web mirroring; such provisions may also hinder code re-use. Conformant licenses must allow for the possibility that (a) redistribution of the software will take place over non-Web channels that do not support click-wrapping of the download, and that (b) the covered code (or re-used portions of covered code) may run in a non-GUI environment that cannot support popup dialogues. </i></p>
		</div>";
	}	
	elseif($_GET[action] == 'gpl')
	{
			$main = "<div class=heading>GNU General Public License</div>
			<div align=justify>
			<p>0. This License applies to any program or other work which contains a notice placed by the copyright holder saying it may be distributed under the terms of this General Public License. The 'Program', below, refers to any such program or work, and a 'work based on the Program' means either the Program or any derivative work under copyright law: that is to say, a work containing the Program or a portion of it, either verbatim or with modifications and/or translated into another language. (Hereinafter, translation is included without limitation in the term 'modification'.) Each licensee is addressed as 'you'.
			<p>Activities other than copying, distribution and modification are not covered by this License; they are outside its scope. The act of running the Program is not restricted, and the output from the Program is covered only if its contents constitute a work based on the Program (independent of having been made by running the Program). Whether that is true depends on what the Program does.
			<p>1. You may copy and distribute verbatim copies of the Program's source code as you receive it, in any medium, provided that you conspicuously and appropriately publish on each copy an appropriate copyright notice and disclaimer of warranty; keep intact all the notices that refer to this License and to the absence of any warranty; and give any other recipients of the Program a copy of this License along with the Program.
			<p>You may charge a fee for the physical act of transferring a copy, and you may at your option offer warranty protection in exchange for a fee.
			<p>2. You may modify your copy or copies of the Program or any portion of it, thus forming a work based on the Program, and copy and distribute such modifications or work under the terms of Section 1 above, provided that you also meet all of these conditions:
			<p>a) You must cause the modified files to carry prominent notices stating that you changed the files and the date of any change.
			<p>b) You must cause any work that you distribute or publish, that in whole or in part contains or is derived from the Program or any part thereof, to be licensed as a whole at no charge to all third parties under the terms of this License.
			<p>c) If the modified program normally reads commands interactively when run, you must cause it, when started running for such interactive use in the most ordinary way, to print or display an announcement including an appropriate copyright notice and a notice that there is no warranty (or else, saying that you provide a warranty) and that users may redistribute the program under these conditions, and telling the user how to view a copy of this License. (Exception: if the Program itself is interactive but does not normally print such an announcement, your work based on the Program is not required to print an announcement.)
			<p>These requirements apply to the modified work as a whole. If identifiable sections of that work are not derived from the Program, and can be reasonably considered independent and separate works in themselves, then this License, and its terms, do not apply to those sections when you distribute them as separate works. But when you distribute the same sections as part of a whole which is a work based on the Program, the distribution of the whole must be on the terms of this License, whose permissions for other licensees extend to the entire whole, and thus to each and every part regardless of who wrote it.
			<p>Thus, it is not the intent of this section to claim rights or contest your rights to work written entirely by you; rather, the intent is to exercise the right to control the distribution of derivative or collective works based on the Program.
			<p>In addition, mere aggregation of another work not based on the Program with the Program (or with a work based on the Program) on a volume of a storage or distribution medium does not bring the other work under the scope of this License.
			<p>3. You may copy and distribute the Program (or a work based on it, under Section 2) in object code or executable form under the terms of Sections 1 and 2 above provided that you also do one of the following: a) Accompany it with the complete corresponding machine-readable source code, which must be distributed under the terms of Sections 1 and 2 above on a medium customarily used for software interchange; or,
			<p>b) Accompany it with a written offer, valid for at least three years, to give any third party, for a charge no more than your cost of physically performing source distribution, a complete machine-readable copy of the corresponding source code, to be distributed under the terms of Sections 1 and 2 above on a medium customarily used for software interchange; or,
			<p>c) Accompany it with the information you received as to the offer to distribute corresponding source code. (This alternative is allowed only for noncommercial distribution and only if you received the program in object code or executable form with such an offer, in accord with Subsection b above.)
			<p>The source code for a work means the preferred form of the work for making modifications to it. For an executable work, complete source code means all the source code for all modules it contains, plus any associated interface definition files, plus the scripts used to control compilation and installation of the executable. However, as a special exception, the source code distributed need not include anything that is normally distributed (in either source or binary form) with the major components (compiler, kernel, and so on) of the operating system on which the executable runs, unless that component itself accompanies the executable.
			<p>If distribution of executable or object code is made by offering access to copy from a designated place, then offering equivalent access to copy the source code from the same place counts as distribution of the source code, even though third parties are not compelled to copy the source along with the object code.
			<p>4. You may not copy, modify, sublicense, or distribute the Program except as expressly provided under this License. Any attempt otherwise to copy, modify, sublicense or distribute the Program is void, and will automatically terminate your rights under this License. However, parties who have received copies, or rights, from you under this License will not have their licenses terminated so long as such parties remain in full compliance.
			<p>5. You are not required to accept this License, since you have not signed it. However, nothing else grants you permission to modify or distribute the Program or its derivative works. These actions are prohibited by law if you do not accept this License. Therefore, by modifying or distributing the Program (or any work based on the Program), you indicate your acceptance of this License to do so, and all its terms and conditions for copying, distributing or modifying the Program or works based on it.
			<p>6. Each time you redistribute the Program (or any work based on the Program), the recipient automatically receives a license from the original licensor to copy, distribute or modify the Program subject to these terms and conditions. You may not impose any further restrictions on the recipients' exercise of the rights granted herein. You are not responsible for enforcing compliance by third parties to this License.
			<p>7. If, as a consequence of a court judgment or allegation of patent infringement or for any other reason (not limited to patent issues), conditions are imposed on you (whether by court order, agreement or otherwise) that contradict the conditions of this License, they do not excuse you from the conditions of this License. If you cannot distribute so as to satisfy simultaneously your obligations under this License and any other pertinent obligations, then as a consequence you may not distribute the Program at all. For example, if a patent license would not permit royalty-free redistribution of the Program by all those who receive copies directly or indirectly through you, then the only way you could satisfy both it and this License would be to refrain entirely from distribution of the Program.
			<p>If any portion of this section is held invalid or unenforceable under any particular circumstance, the balance of the section is intended to apply and the section as a whole is intended to apply in other circumstances.
			<p>It is not the purpose of this section to induce you to infringe any patents or other property right claims or to contest validity of any such claims; this section has the sole purpose of protecting the integrity of the free software distribution system, which is implemented by public license practices. Many people have made generous contributions to the wide range of software distributed through that system in reliance on consistent application of that system; it is up to the author/donor to decide if he or she is willing to distribute software through any other system and a licensee cannot impose that choice.
			<p>This section is intended to make thoroughly clear what is believed to be a consequence of the rest of this License.
			<p>8. If the distribution and/or use of the Program is restricted in certain countries either by patents or by copyrighted interfaces, the original copyright holder who places the Program under this License may add an explicit geographical distribution limitation excluding those countries, so that distribution is permitted only in or among countries not thus excluded. In such case, this License incorporates the limitation as if written in the body of this License.
			<p>9. The Free Software Foundation may publish revised and/or new versions of the General Public License from time to time. Such new versions will be similar in spirit to the present version, but may differ in detail to address new problems or concerns.
			<p>Each version is given a distinguishing version number. If the Program specifies a version number of this License which applies to it and 'any later version', you have the option of following the terms and conditions either of that version or of any later version published by the Free Software Foundation. If the Program does not specify a version number of this License, you may choose any version ever published by the Free Software Foundation.
			<p>10. If you wish to incorporate parts of the Program into other free programs whose distribution conditions are different, write to the author to ask for permission. For software which is copyrighted by the Free Software Foundation, write to the Free Software Foundation; we sometimes make exceptions for this. Our decision will be guided by the two goals of preserving the free status of all derivatives of our free software and of promoting the sharing and reuse of software generally.
			<p>NO WARRANTY </p>
			<p>11. BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW. EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM 'AS IS' WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU. SHOULD THE PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.
			<p>12. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR REDISTRIBUTE THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES, INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED TO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. </p>
			</div>";
	}
	elseif($_GET[action] == 'help')
	{
		$page = "help";
		$main = "
		<div class=heading>Version Information</div>
		<div align=left>
		<ul>
		<li>OpenNewsletter v2.5 {Released on: Friday, December 16, 2006}</li>
		</ul>
		
		<div class=heading><font color=red>New Features</font></div>
		<ul>
		<li>Preview mail before sending</li>
		<li>Send mails through SMTP server</li>
		<li>Mail will be delievrd to the inbox</li>
		</ul>
		
		<div class=heading>Features</div>
		<ul>
		<li>Free & Open Source</li>
		<li>Small, simple, and beautiful</li>
		<li>Requires NO database</li>
		<li>You can add/delete subscribe</li>
		<li>You can import/export subscribe</li>
		<li>Users can subscribe</li>
		<li>Users can unsubscribe (an unsubscribelink may appear at the end of newsletters.) </li>
		<li>Rename database file 'data.dat' to anything for security. </li>
		<li>Send susubscribe link if you want</li>
		<li>Send text or HTML newsletter</li>
		</ul>
		
		<div class=heading>Installation</div>
		<ul>
		<li>Windows: upload at your website and its running.</li>
		<li>Linux: Upload at your website and chmod data.php and config.php to 777</li>
		</ul>
		
		<div class=heading>Integration</div>
		<blockquote>
		<div align=left style='line-height:1.5;'>
		If youe website is
		http://www.mysite.com and you have uploaded OpenNewsletter in a folder 'OpenNewsletter', write this line anywhere in your website:
		<br><br>		
		<div style='background-color:#eee; padding:5px;'>
			&lt;iframe src=\"http://www.mysite.com/OpenNewsletter/includethis.php\" frameborder=\"0\" width=\"200\" height=\"100\" style=\"border:1px solid #CCCCCC\"&gt;&lt;/iframe&gt;
		</div>
		<br>
		It will produce this box. 
		<br>
		<iframe src='includethis.php' frameborder='0' width=200 height=100 style='border:1px solid #CCCCCC'></iframe>
		<br><br>
		
		
		</div>
		</blockquote>
						
		<div class=heading>Developer</div>
		<blockquote>
		<table width=100% cellpadding=3>
		<tr bgcolor=#f9f9f9>
		<td width=150>Name</td><td>Sohail Abid</td>
		</tr>
		<tr bgcolor=#f4f4f4>
		<td>Email</td><td>self.exile {at} gmail {dot} com</td>
		</tr>
		<tr bgcolor=#f9f9f9>
		<td>MSN Messenger</td><td>self.exile {at} hotmail {dot} com</td>
		</tr>
		<tr bgcolor=#f4f4f4>
		<td>Google Talk</td><td>self.exile {at} gmail {dot} com</td>
		</tr>
		<tr bgcolor=#f9f9f9>
		<td>Website</td><td><a target=_blanck href='http://selfexile.com'>selfexile.com</a></td>
		</tr>
		<tr bgcolor=#f4f4f4>
		<td>Flickr Photos</td><td><a target=_blanck href='http://flickr.com/photos/selfexile/'>flickr.com/photos/selfexile</a></td>
		</tr>
		<tr bgcolor=#f9f9f9>
		<td>del.icio.us Bookmarks</td><td><a target=_blanck href='http://del.icio.us/sohailabid/'>del.icio.us/sohailabid</a></td>
		</tr>
		<tr bgcolor=#f4f4f4>
		<td>Technorati Profile</td><td><a target=_blanck href='http://technorati.com/profile/sohailabid/'>technorati.com/profile/sohailabid</a></td>
		</tr>
		</table>
		</blockquote>
		</div>		
		</div>";
	}
	else
	{
		if($_SESSION["valid"] == true)
		{
			$main = "
			<div class=heading>Welcome</div>
			<div align=left style='line-height:1.5;'>
			<b>OpenNewsletter</b> is a free/simple/beautiful open source newsletter or mailing-list 
			manager written in php. It does not require a database. You just need to upload it and its running!
			<br><br>
			This script is developed by me [Sohail Abid]. I'm a freelance open source developer from Pakistan. 
			It is developed because I like simple and beautiful things.
			<br><br>
			This script is part of a dream or movement: <a href='http://freesimpleandbeautiful.com'>Free, Simple, and Beautiful Internet</a> which has three mottos:
			<ul>
			<li><b>Keep the Internet Free</b>
			<li><b>Keep the Internet Simple</b>
			<li><b>Keep the Internet Beautiful</b>
			</ul>
			You can be a part of the movement too if you believe in above 3 phrases.
			<br><br>
			Visit my websites <a href='http://selfexile.com/'>www.selfexile.com</a> and 
			<a href=http://freesimpleandbeautiful.com/'>www.freesimpleandbeautiful.com</a> for more details.
			Home page of this script is <a href='http://selfexile.com/projects/opennewsletter/'>www.selfexile.com/projects/opennewsletter</a>
			and my email is self.exile {at} gmail {dot} com
			</div>
			";
		}
		else
		{
			if($_GET["msg"] == "error")
				$main .= "<div class=error>Access Denied!<br>Invalid Login/Password. Please try again...</div><br>";
				
			$main .= "
			<div class=heading>Welcome</div>
			<div style='text-align:left;'>
			
			<form action='index.php?action=login' method='post'>
			<P style='float:right;width: 100px; padding: 1em; margin: 0;border: 1px solid #CCCCCC; background-color: #efefef;'>
			<b>Admin Login</b>
			<br><br>
			Username<br>
			<input class=textField3 type=text name=username><br><br>
			Password<br>
			<input class=textField3 type=password name=password><br><br>
			<input class=button type=submit value=Login>
			</P>
			</form>
			
			<div align=left style='line-height:1.5;'>
			<b>OpenNewsletter</b> is a free/simple/beautiful open source newsletter or mailing-list 
			manager written in php. It does not require a database. You just need to upload it and its running!
			<br><br>
			This script is developed by me [Sohail Abid]. I'm a freelance open source developer from Pakistan. 
			It is developed because I like simple and beautiful things.
			<br><br>
			This script is part of a dream or movement: <a href='http://freesimpleandbeautiful.com'>Free, Simple, and Beautiful Internet</a> which has three mottos:
			<ul>
			<li><b>Keep the Internet Free</b>
			<li><b>Keep the Internet Simple</b>
			<li><b>Keep the Internet Beautiful</b>
			</ul>
			You can be a part of the movement too if you believe in above 3 phrases.
			<br><br>
			Visit my websites <a href='http://selfexile.com/'>www.selfexile.com</a> or 
			<a href=http://freesimpleandbeautiful.com/'>www.freesimpleandbeautiful.com</a> for more details.
			Home page of this script is <a href='http://selfexile.com/projects/opennewsletter/'>www.selfexile.com/projects/opennewsletter</a>
			and my email is self.exile {at} gmail {dot} com
			</div>
			</DIV>
			";
		}
		$page = "home";
	}
	
	require_once("includes/template.php");
?>